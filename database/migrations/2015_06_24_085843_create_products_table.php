<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('city_id');
			$table->integer('store_id');
			$table->string('slug');
			$table->string('product_image')->default('coffee.png');
			$table->string('product_name')->nullable();
			$table->text('product_description')->nullable();
			$table->decimal('small_amount')->nullable();
			$table->decimal('medium_amount')->nullable();
			$table->decimal('large_amount')->nullable();
			$table->integer('active')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
