<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\Product;

    class ProductsTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $product = new Product();
            $newproduct = $product->create([
                'city_id'               => 2,
                'store_id'              => 1,
                'slug'                  => 'filter-cofee',
                'product_name'          => 'Filter Coffee',
                'product_description'   => 'Filter Coffee Descripton',
                'small_amount'          => '1.09',
                'medium_amount'         => '2.09',
                'large_amount'          => '3.09',
                'active'                => 1,
                'created_at'            => $now,
                'updated_at'            => $now,
            ]);
            $newproduct = $product->create([
                'city_id'               => 2,
                'store_id'              => 1,
                'slug'                  => 'americano',
                'product_name'          => 'Americano',
                'product_description'   => 'Americano Coffee Descripton',
                'small_amount'          => '1.09',
                'medium_amount'         => '2.09',
                'large_amount'          => '3.09',
                'active'                => 1,
                'created_at'            => $now,
                'updated_at'            => $now,
            ]);
            $newproduct = $product->create([
                'city_id'               => 1,
                'store_id'              => 1,
                'slug'                  => 'espresso',
                'product_name'          => 'Espressso',
                'product_description'   => 'Espresso Descripton',
                'small_amount'          => '1.09',
                'large_amount'          => '3.09',
                'active'                => 1,
                'created_at'            => $now,
                'updated_at'            => $now,
            ]);
            $newproduct = $product->create([
                'city_id'               => 2,
                'store_id'              => 1,
                'slug'                  => 'double-espresso',
                'product_name'          => 'Double Espresso',
                'product_description'   => 'Double Espresso Coffee Descripton',
                'small_amount'          => '1.09',
                'medium_amount'         => '2.09',
                'large_amount'          => '3.09',
                'active'                => 1,
                'created_at'            => $now,
                'updated_at'            => $now,
            ]);
            $newproduct = $product->create([
                'city_id'               => 3,
                'store_id'              => 1,
                'slug'                  => 'capuccino',
                'product_name'          => 'DCapuccino',
                'product_description'   => 'Capuccino Coffee Descripton',
                'small_amount'          => '1.09',
                'medium_amount'         => '2.09',
                'large_amount'          => '3.09',
                'active'                => 1,
                'created_at'            => $now,
                'updated_at'            => $now,
            ]);
            $newproduct = $product->create([
                'city_id'               => 1,
                'store_id'              => 1,
                'slug'                  => 'cafe-latte',
                'product_name'          => 'Cafe Latte',
                'product_description'   => 'Cafe Latte Coffee Descripton',
                'small_amount'          => '1.09',
                'medium_amount'         => '2.09',
                'active'                => 1,
                'created_at'            => $now,
                'updated_at'            => $now,
            ]);
            $newproduct = $product->create([
                'city_id'               => 2,
                'store_id'              => 1,
                'slug'                  => 'machiatoo',
                'product_name'          => 'Machiatoo',
                'product_description'   => 'Machiatto Coffee Descripton',
                'medium_amount'         => '2.09',
                'large_amount'          => '3.09',
                'active'                => 1,
                'created_at'            => $now,
                'updated_at'            => $now,
            ]);
            $newproduct = $product->create([
                'city_id'               => 2,
                'store_id'              => 1,
                'slug'                  => 'caramel-machiatoo',
                'product_name'          => 'Caramel Machiatoo',
                'product_description'   => 'Caramel Machiatto Coffee Descripton',
                'small_amount'          => '1.09',
                'medium_amount'         => '2.09',
                'large_amount'          => '3.09',
                'active'                => 1,
                'created_at'            => $now,
                'updated_at'            => $now,
            ]);

        }
    }
?>