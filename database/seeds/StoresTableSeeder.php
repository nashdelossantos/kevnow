<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\Store;

    class StoresTableSeeder extends Seeder{

        public function run()
        {
            $now = date('Y-m-d H:i:s');

            //$user = User::select('id')->where('name', '=', 'store')->first();

            $store = new Store();
            $store->userid              = 2;
            $store->slug                = 'new-store-name';
            $store->store_logo          = 'no-image.jpg';
            $store->store_name          = 'New Store Name';
            $store->store_description   = 'This is a new store';
            $store->address             = 'New Store Address';
            $store->email               = 'newstore@email.com';
            $store->phone               = '32453545';
            $store->save();
        }
    }
?>