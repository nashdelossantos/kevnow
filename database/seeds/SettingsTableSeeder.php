<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\Setting;

    class SettingsTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $options = new Setting();
            $newOption = $options->create([
                'slug'              => 'site_name',
                'settings_name'     => 'Site Name',
                'settings_value'    => 'Kevnow'
            ]);
            $newOption = $options->create([
                'slug'              => 'footer',
                'settings_name'     => 'Footer',
                'settings_value'    => 'Copyright Kevnow, Inc'
            ]);
            $newOption = $options->create([
                'slug'              => 'front-title',
                'settings_name'     => 'Front Page Title',
                'settings_value'    => '$5 FAST COFFEE DELIVERY'
            ]);
        }
    }
?>