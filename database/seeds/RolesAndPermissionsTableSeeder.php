<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\Role;
    use App\User;
    use App\Permission;

    class RolesAndPermissionsTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $admin = new Role();
            $admin->name         = 'admin';
            $admin->display_name = 'Administrator'; // optional
            $admin->description  = 'User is allowed to manage and edit other users'; // optional
            $admin->save();

            $customer = new Role();
            $customer->name         = 'customer';
            $customer->display_name = 'Customer'; // optional
            $customer->description  = 'Standard customer'; // optional
            $customer->save();

            $store = new Role();
            $store->name         = 'store';
            $store->display_name = 'Store Owner'; // optional
            $store->description  = 'Can review and update orders'; // optional
            $store->save();

            $driver = new Role();
            $driver->name         = 'driver';
            $driver->display_name = 'Driver'; // optional
            $driver->description  = 'View accept and update order status'; // optional
            $driver->save();

            //assign roles
            $userAdmin = User::where('name', '=', 'admin')->first();
            $userAdmin->attachRole($admin);

            $userCustomer = User::where('name', '=', 'customer')->first();
            $userCustomer->attachRole($customer);

            $userStore = User::where('name', '=', 'store')->first();
            $userStore->attachRole($store);

            $userDriver = User::where('name', '=', 'driver')->first();
            $userDriver->attachRole($driver);

            //add permissions

            //admin
            $createProduct = new Permission();
            $createProduct->name         = 'create-product';
            $createProduct->display_name = 'Create Product'; // optional
            $createProduct->description  = 'create new product'; // optional
            $createProduct->save();

            $deleteProduct = new Permission();
            $deleteProduct->name         = 'delete-product';
            $deleteProduct->display_name = 'Delete Product'; 
            $deleteProduct->description  = 'delete product';
            $deleteProduct->save();

            $updateOrder = new Permission();
            $updateOrder->name         = 'update-order';
            $updateOrder->display_name = 'Update Order'; 
            $updateOrder->description  = 'update order';
            $updateOrder->save();

            $refundOrder = new Permission();
            $refundOrder->name         = 'refund-order';
            $refundOrder->display_name = 'Refund Order'; 
            $refundOrder->description  = 'refund order';
            $refundOrder->save();

            $cancelOrder = new Permission();
            $cancelOrder->name         = 'cancel-order';
            $cancelOrder->display_name = 'Cancel Order'; 
            $cancelOrder->description  = 'cancel order';
            $cancelOrder->save();

            $createOrder = new Permission();
            $createOrder->name         = 'create-order';
            $createOrder->display_name = 'Create Order'; 
            $createOrder->description  = 'create order';
            $createOrder->save();

            //store permission
            $reviewOrder = new Permission();
            $reviewOrder->name         = 'review-order';
            $reviewOrder->display_name = 'Review Order'; 
            $reviewOrder->description  = 'review order';
            $reviewOrder->save();

            //driver permissions
            $viewOrder = new Permission();
            $viewOrder->name         = 'view-order';
            $viewOrder->display_name = 'View Order'; 
            $viewOrder->description  = 'view order';
            $viewOrder->save();

            $acceptOrder = new Permission();
            $acceptOrder->name         = 'accept-order';
            $acceptOrder->display_name = 'Accept Order'; 
            $acceptOrder->description  = 'accept order';
            $acceptOrder->save();

            $updateStatus = new Permission();
            $updateStatus->name         = 'update-status';
            $updateStatus->display_name = 'Update Status'; 
            $updateStatus->description  = 'update status';
            $updateStatus->save();

            //customer permissions
            $referFriend = new Permission();
            $referFriend->name         = 'refer-friend';
            $referFriend->display_name = 'Refer Friend'; 
            $referFriend->description  = 'refer friend';
            $referFriend->save();

            //attach permision to admin user
            $admin->attachPermissions([$createProduct, $deleteProduct, $updateOrder, $refundOrder, $cancelOrder, $createOrder]);
            $store->attachPermissions([$reviewOrder, $updateOrder]);
            $driver->attachPermissions([$viewOrder, $acceptOrder, $updateStatus]);
            $customer->attachPermission($referFriend);

        }
    }
?>