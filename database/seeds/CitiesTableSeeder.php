<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\City;

    class CitiesTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $city = new City();
            $newcity = $city->create([
                'slug'              => 'austin',
                'city_name'         => 'Austin',
                'city_description'  => 'Austin City'
            ]);
            $newcity = $city->create([
                'slug'              => 'dallas',
                'city_name'         => 'Dallas',
                'city_description'  => 'Houston City'
            ]);
            $newcity = $city->create([
                'slug'              => 'houston',
                'city_name'         => 'Houston',
                'city_description'  => 'Houston City'
            ]);

        }
    }
?>