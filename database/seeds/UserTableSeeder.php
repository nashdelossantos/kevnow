<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\User;

    class UserTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $user = new User();
            $userAdmin = $user->create([
                'name'          => 'admin',
                'email'         => 'info@codegap.co.uk',
                'password'      => Hash::make('pa55w0rd'),
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);
            
            $userModerator = $user->create([
                'name'          => 'customer',
                'email'         => 'customer@codegap.co.uk',
                'password'      => Hash::make('password'),
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

            $userClient = $user->create([
                'name'          => 'store',
                'email'         => 'store@codegap.co.uk',
                'password'      => Hash::make('password'),
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

            $userClient = $user->create([
                'name'          => 'driver',
                'email'         => 'driver@codegap.co.uk',
                'password'      => Hash::make('password'),
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

        }
    }
?>