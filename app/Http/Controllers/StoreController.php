<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Store;
use Redirect;

class StoreController extends Controller
{

    protected $pagename;

    public function __construct()
    {
        $this->pagename = 'stores';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $getstores = Store::orderBy('store_name')->get();

        return view('admin.stores.stores_index',
            [
                'pagename'  => $this->pagename,
                'storelist' => $getstores
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.stores.stores_create',
            [
                'pagename' => $this->pagename
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $store = new Store;
        $store->userid              = Auth::id();
        $store->slug                = str_replace(' ', '-', strtolower($request->store_name));
        $store->store_name          = $request->store_name;
        $store->store_description   = $request->store_description;
        $store->address             = $request->address;
        $store->email               = $request->email;
        $store->phone               = $request->phone;

        if ($request->hasFile('store_logo')) {
            $image = $request->file('store_logo');
            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('img/logos/');
            $image->move($path, $filename);
            //save filename to db
            $store->store_logo = $filename;
        } 

        $store->save();

        return Redirect::to('/admin/stores');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return 'show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug
     * @return Response
     */
    public function edit($slug)
    {
        $getstore = Store::whereSlug($slug)->first();

        return view('admin.stores.stores_edit',
            [
                'pagename'  => $this->pagename,
                'store'     => $getstore
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  string  $slug
     * @return Response
     */
    public function update(Request $request, $slug)
    {
        $store = Store::whereSlug($slug)->first();
        //$store->userid              = Auth::id();
        $store->slug                = str_replace(' ', '-', strtolower($request->store_name));
        $store->store_name          = $request->store_name;
        $store->store_description   = $request->store_description;
        $store->address             = $request->address;
        $store->email               = $request->email;
        $store->phone               = $request->phone;

        if ($request->hasFile('store_logo')) {
            $image = $request->file('store_logo');
            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('img/logos/');
            $image->move($path, $filename);
            //save filename to db
            $store->store_logo = $filename;
        } 

        $store->save();

        return Redirect::to('/admin/stores');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return Response
     */
    public function destroy($slug)
    {
        $store = Store::whereSlug($slug)->first();
        $store->delete();

        return Redirect::to('/admin/stores');
    }
}
