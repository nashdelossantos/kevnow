<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\City;
use Redirect;

class CitiesController extends Controller {

	protected $pagename;

	public function __construct()
	{
		$this->pagename = 'cities';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$getcities = City::orderBy('city_name', 'asc')->get();

		return view('admin.cities.cities_index',
			[
				'citylist' 		=> $getcities,
				'pagename' 		=> $this->pagename
			]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.cities.cities_create',
			[
				'pagename' 		=> $this->pagename
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$city = new City;
		$city_name 	= $request->city_name;
		$slug 		= str_replace(' ', '-', strtolower($city_name));

		$now = date('Y-m-d H:i:s');

		$city->slug 			= $slug;
		$city->city_name 		= $city_name;
		$city->city_description	= $request->city_description;
		$city->created_at 		= $now;
		$city->updated_at 		= $now;
		$city->save();

		return Redirect::to('/admin/cities');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function edit($slug)
	{
		$getcity = City::whereSlug($slug)->first();

		return view('admin.cities.cities_edit',
			[
				'pagename' 	=> $this->pagename,
				'city' 		=> $getcity
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function update(Request $request, $slug)
	{
		$city = City::whereSlug($slug)->first();

		$now = date('Y-m-d H:i:s');

		$city->slug 				= str_replace(' ', '-', strtolower($request->city_name));
		$city->city_name 			= $request->city_name;
		$city->city_description 	= $request->city_description;
		$city->updated_at 			= $now;
		$city->save();

		return Redirect::to('/admin/cities');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function destroy($slug)
	{
		$getcity = City::whereSlug($slug)->first();

		$getcity->delete();

		return Redirect::to('/admin/cities');
	}

}
