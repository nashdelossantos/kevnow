<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\City;

class homeController extends Controller
{

    public function index()
    {
        return view( 'home' );
    }

    public function city( $city )
    {
    	$getcityid = City::whereSlug($city)->first();

    	$getproducts = DB::table('products')
            ->select('*')
            ->join('cities', 'products.city_id', '=', 'cities.id')
            ->orderBy('products.product_name', 'asc')
            ->where('products.city_id', '=', $getcityid->id)
            ->get();

        return view('orders.coffee-types',
        	[
        		'coffeelist' 	=> $getproducts
        	]);
    }

    
}
