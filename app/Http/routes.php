<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get( '/', 'HomeController@index' );
Route::get( 'city/{city}', 'HomeController@city' );
Route::get( 'order/{name}/{size}' , 'orderController@placedOrder' );
Route::get( 'order/confirmation' , 'orderController@confirmOrder' );
Route::get( 'order/payment' , 'orderController@processPayment' );
Route::get( 'order/success' , 'orderController@paymentSuccess' );

//protect admin routes
Route::get('/admin/dashboard', 'AdminController@index');
Entrust::routeNeedsRole('admin/*', ['admin', 'owner'], Redirect::to('/forbidden'), false);

Route::resource('/admin/stores', 'StoreController');
Route::resource('/admin/pages', 'AdminPagesController');
Route::resource('/admin/orders', 'AdminOrdersController');

Route::resource('/admin/customers', 'CustomersController');
Route::resource('/admin/settings', 'SettingsController');
Route::resource('/admin/cities', 'CitiesController');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);