<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;


class Product extends Eloquent
{
	protected $fillable = [ 'city_id', 'store_id', 'slug', 'product_name', 'product_description', 'small_amount', 'medium_amount', 'large_amount'];
}