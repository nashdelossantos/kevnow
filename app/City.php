<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;


class City extends Eloquent
{
	protected $fillable = ['slug', 'city_name', 'city_description'];
}