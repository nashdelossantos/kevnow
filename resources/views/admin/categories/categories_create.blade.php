@extends('admin.masters.base')

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/awesome-bootstrap-checkbox.css') }}">
@stop

@section('pagetitle')
	Categories <small>new entry</small>
@stop

@section('crumbs')
	<li><a href="{{ url('admin/categories') }}"><i class="fa fa-th"></i> Categories</a></li>
	<li class="active"><i class="fa fa-pencil"></i> New Category</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Create New Category</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/categories') }}" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> Cancel</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
            			<div class="col-md-5">
            				{!! Form::open(['url' => '/admin/categories']) !!}
            					<div class="form-group">
            						{!! Form::label('catname', 'Category Name', ['class' => 'control-label']) !!}
									{!! Form::text('category_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            					</div>
									
								<div class="form-group">
									{!! Form::label('order', 'Order Number', ['class' => 'control-label']) !!}
									<div class="input-group spinner">
                                        {!! Form::text('order', 0, ['class' => 'form-control']) !!}
                                        <div class="input-group-btn-vertical">
                                            <div class="btn btn-default"><i class="fa fa-caret-up"></i></div>
                                            <div class="btn btn-default"><i class="fa fa-caret-down"></i></div>
                                        </div>
                                    </div>
								</div>

								<div class="form-group">
									<div class="checkbox checkbox-success">
										<input type="checkbox" id="status" name="status">
											<label for="checkbox1"> Tick to make public
										</label>
									</div>
								</div>
								
								<div class="form-group text-right">
									{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
								</div>
								
            				{!! Form::close() !!}
            			</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@stop

@section('scripts')
    <script>
            $(document).ready(function(){
        //Stop people from typing
        $('.spinner input').keydown(function(e){
            e.preventDefault();
            return false;
        });
        var minNumber = 1;
        var maxNumber = 10;
        $('.spinner .btn:first-of-type').on('click', function() {
            if($('.spinner input').val() == maxNumber){
                return false;
            }else{
                $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
            }
        });

        $('.spinner .btn:last-of-type').on('click', function() {
            if($('.spinner input').val() == minNumber){
                return false;
            }else{
                $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
            }
        });
    });
    </script>
@stop


