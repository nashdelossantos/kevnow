@extends('admin.masters.base')

@section('pagetitle')
	Categories <small>overview of categories</small>
@stop

@section('crumbs')
	<li class="active">
		<i class="fa fa-th"></i> Categories
	</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Category Panel</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/categories/create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Category</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category Name</th>
                                    <th>Order of appearance</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach ($categories as $key => $category)
	                                <tr id="{{ $category->id }}">
	                                    <td>{{ $key }}</td>
	                                    <td>{{ $category->category_name }}</td>
	                                    <td>{{ $category->order }}</td>
	                                    <td class="text-center">@if ($category->status == 0) <span class="label label-default">inactive</span> @else <span class="label label-success">active</span> @endif</td>
	                                    <td class="text-center">
	                                    	<div class="btn-group">
	                                    		<button class="btn btn-warning btn-sm deltriggerbtn" data-toggle="modal" data-target="#myModal" data-slug="{{ $category->slug }}"><i class="fa fa-trash"></i></button>
	                                    		<a href="{{ url('/admin/categories/') }}/{{ $category->slug }}/edit" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
	                                    	</div>
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Deletion</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info">
                        <p>Are you sure you wan to delete this category?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    
                    {!! Form::open(['method' => 'DELETE', 'id' => 'categoryDelete', 'route' => ['admin.categories.destroy', 'test']]) !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
    
                    
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function() {
            $('button.deltriggerbtn').click(function(e){
                var slug = $(this).data('slug');

                $('#categoryDelete').attr('action', '{{ url("/admin/categories/") }}'+'/'+slug );
            });

            //delete a category

        });
    </script>
@stop