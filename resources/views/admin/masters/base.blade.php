<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Panel</title>

    <link rel="stylesheet" href="{{ asset('admin/css/bootstrap.min.css') }}">
    <link href="{{ asset('admin/css/sb-admin.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('admin/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/content-styles.css') }}">

    @yield('styles')

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Store Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
          
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-fw fa-user"></i> Profile</a></li>
                        <li><a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li @if ($pagename == 'dashboard') class="active" @endif>
                        <a href="{{ url('/admin/dashboard') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li @if ($pagename == 'stores') class="active" @endif><a href="{{ url('/admin/stores') }}"><i class="fa fa-fw fa-credit-card"></i> Stores</a></li>
                    <li @if ($pagename == 'pages') class="active" @endif><a href="{{ url('/admin/pages') }}"><i class="fa fa-fw fa-file-o"></i> Pages</a></li>
                    <li @if ($pagename == 'cities') class="active" @endif><a href="{{ url('/admin/cities') }}"><i class="fa fa-fw fa-map-marker"></i> Cities</a></li>
                    <li @if ($pagename == 'customers') class="active" @endif><a href="{{ url('/admin/customers') }}"><i class="fa fa-fw fa-group"></i> Customers</a></li>
                    <li><a href="{{ url('/admin/drivers') }}"><i class="fa fa-fw fa-truck"></i> Drivers</a></li>
                    <li><a href="{{ url('/admin/orders') }}"><i class="fa fa-fw fa-shopping-cart"></i> Orders</a></li>
                    <li @if ($pagename == 'settings') class="active" @endif><a href="{{ url('/admin/settings') }}"><i class="fa fa-fw fa-cogs"></i> Settings</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> @yield('pagetitle') </h1>
                        <ol class="breadcrumb"> @yield('crumbs') </ol>
                    </div>
                </div>

                @yield('pagecontents')


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    @yield('scripts')

</body>

</html>
