@extends('admin.masters.base')

@section('pagetitle')
	Dashboard <small>Shop overview</small>
@stop

@section('crumbs')
	<li class="active">
		<i class="fa fa-dashboard"></i> Dashboard
	</li>
@stop

@section('pagecontents')
	<div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">26</div>
                            <div>Total Stores</div>
                        </div>
                    </div>
                </div>
                <a href="{{ url('/admin/stores') }}">
                    <div class="panel-footer">
                        <span class="pull-left">View Stores</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-group fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">12</div>
                            <div>Total Customers</div>
                        </div>
                    </div>
                </div>
                <a href="{{ url('/admin/products') }}">
                    <div class="panel-footer">
                        <span class="pull-left">View Customesr</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-truck fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">124</div>
                            <div>Total Drivers</div>
                        </div>
                    </div>
                </div>
                <a href="{{ url('/admin/news') }}">
                    <div class="panel-footer">
                        <span class="pull-left">View Drivers</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">13</div>
                            <div>Total Orders</div>
                        </div>
                    </div>
                </div>
                <a href="{{ url('/admin/orders') }}">
                    <div class="panel-footer">
                        <span class="pull-left">View Orders</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>

@stop