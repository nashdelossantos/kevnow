@extends('admin.masters.base')

@section('pagetitle')
	Products <small>overview of products</small>
@stop

@section('crumbs')
	<li class="active">
		<i class="fa fa-cubes"></i> Products
	</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Product Panel</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/products/create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Product</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body products">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="product-image">Product Image</th>
                                    <th>Product Name</th>
                                    <th>Colour</th>
                                    <th>Dimension</th>
                                    <th>Weight</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                    <th>Orders</th>
                                    <th>Active</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach ($products as $key => $product)
	                                <tr id="{{ $product->id }}">
                                        <td>{{ $key }}</td>
	                                    <td><img src="{{ asset('images/products/') }}/{{ $product->product_image }}" alt="{{ $product->product_name }}"></td>
	                                    <td>{{ $product->product_name }}</td>
                                        <td>{{ $product->colour }}</td>
                                        <td>{{ $product->dimension }}</td>
                                        <td>{{ $product->weight }}</td>
                                        <td>{{ $product->quantity }}</td>
                                        <td>{{ $product->amount }}</td>
	                                    <td>{{ $product->times_ordered }}</td>
	                                    <td class="text-center">@if ($product->active == 0) <span class="label label-default">inactive</span> @else <span class="label label-success">active</span> @endif</td>
	                                    <td class="text-center">
	                                    	<div class="btn-group">
	                                    		<button class="btn btn-warning btn-sm deltriggerbtn" data-toggle="modal" data-target="#myModal" data-slug="{{ $product->slug }}"><i class="fa fa-trash"></i></button>
	                                    		<a href="{{ url('/admin/products/') }}/{{ $product->slug }}/edit" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
	                                    	</div>
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Deletion</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info">
                        <p>Are you sure you wan to delete this product?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    
                    {!! Form::open(['method' => 'DELETE', 'id' => 'productDelete', 'route' => ['admin.products.destroy', 'test']]) !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
    
                    
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function() {
            $('button.deltriggerbtn').click(function(e){
                var slug = $(this).data('slug');

                $('#productDelete').attr('action', '{{ url("/admin/products/") }}'+'/'+slug );
            });

            //delete a category

        });
    </script>
@stop