@extends('admin.masters.base')

@section('pagetitle')
	Pages <small>overview</small>
@stop

@section('crumbs')
	<li class="active">
		<i class="fa fa-text-o"></i> Pages
	</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Page Panel</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/pages/create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Page</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body pages">
                    @if ($pagelist == '')
                        <div class="alert alert-info">Page list is currently empty. </div>
                    @else
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th class="product-image">Page Image</th>
                                        <th>Page Title</th>
                                        <th>Subtitle</th>
                                        <th>Active</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach ($pagelist as $key => $page)
    	                                <tr id="{{ $page->id }}">
                                            <td>{{ $key }}</td>
    	                                    <td><img src="{{ asset('images/') }}/{{ $page->page_image }}" alt="{{ $page->page_title }}"></td>
    	                                    <td>{{ $page->page_title }}</td>
                                            <td>{{ $page->subtitle }}</td>
    	                                    <td class="text-center">@if ($page->active == 0) <span class="label label-default">inactive</span> @else <span class="label label-success">active</span> @endif</td>
    	                                    <td class="text-center">
    	                                    	<div class="btn-group">
    	                                    		<button class="btn btn-warning btn-sm deltriggerbtn" data-toggle="modal" data-target="#myModal" data-slug="{{ $page->slug }}"><i class="fa fa-trash"></i></button>
    	                                    		<a href="{{ url('/admin/pages/') }}/{{ $page->slug }}/edit" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
    	                                    	</div>
    	                                    </td>
    	                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Deletion</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info">
                        <p>Are you sure you wan to delete this Page?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    
                    {!! Form::open(['method' => 'DELETE', 'id' => 'pageDelete', 'route' => ['admin.products.destroy', 'test']]) !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
    
                    
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function() {
            $('button.deltriggerbtn').click(function(e){
                var slug = $(this).data('slug');

                $('#pageDelete').attr('action', '{{ url("/admin/pages/") }}'+'/'+slug );
            });

            //delete a category

        });
    </script>
@stop