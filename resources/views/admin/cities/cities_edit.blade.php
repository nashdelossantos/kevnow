@extends('admin.masters.base')

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/awesome-bootstrap-checkbox.css') }}">
@stop

@section('pagetitle')
	City <small>edit</small>
@stop

@section('crumbs')
	<li><a href="{{ url('admin/pages') }}"><i class="fa fa-map-marker"></i> City</a></li>
	<li class="active"><i class="fa fa-pencil"></i> Editing a City</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Create New City</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/cities') }}" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> Cancel</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        {!! Form::model($city, ['url' => '/admin/cities/' . $city->slug, 'method' => 'PATCH', 'files' => true]) !!}
                			<div class="col-md-8">
                				
            					<div class="form-group">
                                    {!! Form::label('city_name', 'City Name', ['class' => 'control-label']) !!}
                                    {!! Form::text('city_name', null, ['class' => 'form-control', 'required']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('city_description', 'Description', ['class' => 'control-label']) !!}
                                    {!! Form::textarea('city_description', null, ['class' => 'form-control', 'rows' => '12']) !!}
                                </div>
                                
                            </div>

                            <div class="col-md-12">
                                <div class="form-group text-right">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@stop
