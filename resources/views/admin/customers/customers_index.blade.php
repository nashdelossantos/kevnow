@extends('admin.masters.base')

@section('pagetitle')
	Customers <small>overview</small>
@stop

@section('crumbs')
	<li class="active">
		<i class="fa fa-group"></i> Customers
	</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Customers Panel</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/customers/create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Customer</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body pages">
                    @if ($customerslist == '')
                        <div class="alert alert-info">Customer's list is currently empty. </div>
                    @else
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>created at</th>
                                        <th>updated at</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach ($customerslist as $key => $customer)
    	                                <tr id="{{ $customer->id }}">
                                            <td>{{ $key }}</td>
    	                                    <td>{{ $customer->name }}</td>
    	                                    <td>{{ $customer->email }}</td>
                                            <td>{{ date("d-m-Y : ga",strtotime($customer->created_at)) }}</td>
                                            <td>{{ date("d-m-Y : ga",strtotime($customer->updated_at)) }}</td>
    	                                    <td class="text-center">
    	                                    	<div class="btn-group">
    	                                    		<button class="btn btn-warning btn-sm deltriggerbtn" data-toggle="modal" data-target="#myModal" data-slug="{{ $customer->id }}"><i class="fa fa-trash"></i></button>
    	                                    		<a href="{{ url('/admin/customers/') }}/{{ $customer->id }}/edit" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
    	                                    	</div>
    	                                    </td>
    	                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Deletion</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info">
                        <p>Are you sure you wan to delete this Customer?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    
                    {!! Form::open(['method' => 'DELETE', 'id' => 'pageDelete', 'route' => ['admin.customers.destroy', 'test']]) !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
    
                    
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function() {
            $('button.deltriggerbtn').click(function(e){
                var slug = $(this).data('slug');

                $('#pageDelete').attr('action', '{{ url("/admin/customers/") }}'+'/'+slug );
            });

            //delete a category

        });
    </script>
@stop