@extends('admin.masters.base')

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/awesome-bootstrap-checkbox.css') }}">
@stop

@section('pagetitle')
	Customers <small>new entry</small>
@stop

@section('crumbs')
	<li><a href="{{ url('admin/pages') }}"><i class="fa fa-group"></i> Customers</a></li>
	<li class="active"><i class="fa fa-user"></i> New Customer</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Create New Customer</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/customers') }}" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> Cancel</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="alert alert-danger hidden" id="wrongpassword">Password doesn't match!</div>
                    <div class="alert alert-danger hidden" id="emptypassword">Password cannot be empty</div>
                    <div class="table-responsive">
                        {!! Form::open(['url' => '/admin/customers', 'files' => true]) !!}
                			<div class="col-md-8">
                				
            					<div class="form-group">
            						{!! Form::label('name', 'Customer Name', ['class' => 'control-label']) !!}
									{!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
            					</div>

                                <div class="form-group">
                                    {!! Form::label('email', 'Email Address', ['class' => 'control-label']) !!}
                                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password1', 'Password', ['class' => 'control-label']) !!}
                                    {!! Form::password('password1', ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password2', 'Confirm Password', ['class' => 'control-label']) !!}
                                    {!! Form::password('password2', ['class' => 'form-control']) !!}
                                </div>
    								
                			</div>

                            <div class="col-md-12">

                                <div class="form-group text-right">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script>
        $(function() {
            $('input[type=submit]').click(function(e) {
                e.preventDefault();

                var p1 = $('#password1').val();
                var p2 = $('#password2').val();
                var empty = '';

                if (p1 != p2) {
                    $('#wrongpassword').removeClass('hidden');
                    $('#emptypassword').addClass('hidden');
                } else if (p1 == empty || p2 == empty) {
                    $('#emptypassword').removeClass('hidden');
                    $('#wrongpassword').addClass('hidden');
                } else {
                    $('form').submit();
                }
            });
        });
    </script>
@stop
