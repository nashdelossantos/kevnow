@extends('admin.masters.base')

@section('pagetitle')
	Stores <small>overview</small>
@stop

@section('crumbs')
	<li class="active">
		<i class="fa fa-text-o"></i> Stores
	</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Store Panel</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/stores/create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Store</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body pages">
                    @if ($storelist == '')
                        <div class="alert alert-info">Store list is currently empty. </div>
                    @else
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th class="product-image">Logo</th>
                                        <th>Store Name</th>
                                        <th>Description</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach ($storelist as $key => $store)
    	                                <tr id="{{ $store->id }}">
                                            <td>{{ $key }}</td>
    	                                    <td>@if ($store->store_logo == '')<span>No Logo  @else </span><img src="{{ asset('img/logos/') }}/{{ $store->store_logo }}" alt="{{ $store->store_name }}">@endif </td>
    	                                    <td>{{ $store->store_name }}</td>
                                            <td>{{ $store->store_description }}</td>
                                            <td>{{ $store->address }}</td>
                                            <td>
                                                <p><i class="fa fa-envelope"></i> {{ $store->email }}<br/>
                                                    <i class="fa fa-phone"></i> {{ $store->phone }}</p>
                                            </td>
    	                                    <td class="text-center">
    	                                    	<div class="btn-group">
    	                                    		<button class="btn btn-warning btn-sm deltriggerbtn" data-toggle="modal" data-target="#myModal" data-slug="{{ $store->slug }}"><i class="fa fa-trash"></i></button>
    	                                    		<a href="{{ url('/admin/stores/') }}/{{ $store->slug }}/edit" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
    	                                    	</div>
    	                                    </td>
    	                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Deletion</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info">
                        <p>Are you sure you wan to delete this Page?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::open(['method' => 'DELETE', 'id' => 'pageDelete', 'route' => ['admin.stores.destroy']]) !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
    
                    
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function() {
            $('button.deltriggerbtn').click(function(e){
                var slug = $(this).data('slug');

                $('#pageDelete').attr('action', '{{ url("/admin/stores/") }}'+'/'+slug );
            });

            //delete a page

        });
    </script>
@stop