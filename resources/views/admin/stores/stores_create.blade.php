@extends('admin.masters.base')

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/awesome-bootstrap-checkbox.css') }}">
@stop

@section('pagetitle')
	Store <small>new entry</small>
@stop

@section('crumbs')
	<li><a href="{{ url('admin/pages') }}"><i class="fa fa-home"></i> Store</a></li>
	<li class="active"><i class="fa fa-pencil"></i> New Store</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Create New Store</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/stores') }}" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> Cancel</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        {!! Form::open(['url' => '/admin/stores', 'files' => true]) !!}
                			<div class="col-md-8">
                				
            					<div class="form-group">
            						{!! Form::label('store_name', 'Store Name', ['class' => 'control-label']) !!}
									{!! Form::text('store_name', null, ['class' => 'form-control', 'required']) !!}
            					</div>
                			</div>
                            <div class="col-md-4">
                                
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        {!! Form::label('address', 'Address', ['class' => 'control-label']) !!}
                                        {!! Form::textarea('address', null, ['class' => 'form-control', 'rows' => 10]) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('store_logo', 'Store Logo', ['class' => 'control-label']) !!}
                                        {!! Form::file('store_logo') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('phone', 'Phone', ['class' => 'control-label']) !!}
                                        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('email', 'Email Address', ['class' => 'control-label']) !!}
                                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    {!! Form::label('store_description', 'Description', ['class' => 'control-label']) !!}
                                    {!! Form::textarea('store_description', null, ['class' => 'form-control', 'rows' => '12']) !!}
                                </div>

                                <div class="form-group text-right">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@stop
