@extends('base.main')

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/pages.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('content')
<section class="container-fluid main-content" id="coffee-types-content">
	<div class="container">
		<div class="title-heading">
			<h2>Choose type of Coffee:</h2>
		</div>
		<div class="coffee-options">
			@foreach ($coffeelist as $coffee)
				<div class="col-md-3 coffee-description">
					<div class="coffee-img">
						<img src="{{ asset('img/icons/') }}/{{ $coffee->product_image }}" class="product-image" data-desc="{{ $coffee->product_description }}" data-title="{{ $coffee->product_name }}">
					</div>
					<div class="price-details">
						<p class="name">{{ $coffee->product_name }}</p>
						<div class="btn-group" role="group" aria-label="coffee sizes">
							@if ($coffee->small_amount)
								<button type="button" class="btn-size btn btn-default" value="Small">Small</button>
							@endif
							@if ($coffee->medium_amount)
								<button type="button" class="btn-size btn btn-info" value="Medium">Medium</button>
							@endif
							@if ($coffee->large_amount)
								<button type="button" class="btn-size btn btn-primary" value="Large">Large</button>
							@endif
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</section>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/sweetalert.min.js') }}"></script>
<script type="text/javascript">
	$( '.price-details' ).delegate( '.btn-size' , 'click' , function()  {
		//var base_url = window.location.origin + '/~dev5/';
		var base_url = window.location.origin + '/';
		var coffee = {
			'size' : $( this ).val(),
			'name' : $( this ).parent().siblings().text()
		}

		swal( {   
			title:"Order added",   
			text: "You have selected " + coffee.size + " " + coffee.name,   
			type: "warning",   
			showCancelButton: true,   
			cancelButtonText: 'Order More',
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Add Extras",   
			closeOnConfirm: false }, 
			function() {   
				swal("Successfully added!", "Redirecting to checkout." , "success"); 
				setTimeout(function() {
					window.location = base_url + "order/" + coffee.name + "/" + coffee.size ;
				}, 2000);
			} );
		
	} )

	$('.product-image').click(function(e) {
		var title 	= $(this).data('title');
		var desc 	= $(this).data('desc');

		swal(title, desc, "info")
	});
</script>
@endsection
